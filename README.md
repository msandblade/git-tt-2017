# README #

Hi, This repo contains my presentation for Tech Talk Live 2017

Git for Non-Developers
Presenter - Mark Sandblade
Originally presented May 2, 2017

### What's in here? ###

* The original powerpoint slide deck.
* Some HTML docs for some common tips and tricks I use in Git
* Links to some resources that helped me get started with Git

### FAQ ###

* How can I find you? msandblade@lancasterlibraries.org
* How can I find the Library System of Lancaster County? http://lancasterlibraries.org
* Are you a library? No, but we help libraries in Lancaster County, much like the IU helps school districts
* Can I submit a pull request for this repo?  You can, but honestly I might not get around to pulling your stuff... no offense.